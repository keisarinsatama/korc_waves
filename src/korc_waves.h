#ifndef korc_waves_h
#define korc_waves_h

#include "SDL_includet.h"

#include "classes/gfx.h"
#include "classes/snd.h"
#include "classes/player.h"
#include "classes/text.h"
#include "classes/moon.h"


#define GAMESTATE_RUNNING 0
#define GAMESTATE_GAMEOVER 1
#define GAMESTATE_MENU 2

#define PLAYER_RED 0
#define PLAYER_BLUE 1

#define GRAVITY 0.00030
#define OVER_BORDER_SPEED_PENALTY 0.3
#define BOUNCE_FACTOR 0.9

struct world{
	bool exit;
	int gamestate,refresh_time,refresh_rate,winner;
	double gravity;
	double border_speed_penalty,bounce_factor;
	class Player *ship1, *ship2;
	class Moon *moon;
};

void gameloop(world *w, class Gfx *gfx, class Snd *snd, class Text *text);
void mainmenu(world *w, class Gfx *gfx, class Snd *snd, class Text *text);
void gameover(world *w, class Gfx *gfx, class Snd *snd, class Text *text);

#endif
