#include <iostream>
#include <math.h>
#include "../korc_waves.h"
#include "player.h"
#include "snd.h"

Player::Player(Gfx *g,struct world *w,Snd *snd,SDL_Color color,Gfx_Point start_location, double start_dir, Gfx_Point start_delta) {
	this->g=g;
	this->snd=snd;
	this->w=w;

// Ship vertices.
	this->p_orig[0]={0,-0.080};
	this->p_orig[1]={-0.040,0.030};
	this->p_orig[2]={0,0.015};
	this->p_orig[3]={0.040,0.030};
	this->p_orig[4]={0,-0.080};

	this->start_location = start_location;
	this->start_dir = start_dir;
	this->start_delta = start_delta;
	this->color=color;

	this->setScale(0.20);
	this->calcScale();

	this->part=new Particle(this->g,w,500);
	this->particle_effect = this->part->init_error ? false : true; 
	if(this->particle_effect) this->part->setFadeSpeed(500);
}

Player::~Player() {
	delete(this->part);
};

void Player::restart()
{
	this->location=this->start_location;
	this->orientation=0;
	this->direction = this->start_dir;
	this->delta = this->start_delta;
	this->thrust=0;
	this->fuel=100;
}

void Player::setScale(double scale) {	// TODO sanitation
	this->scale=scale;
}

void Player::setThrust(double thrust) {
	if(thrust>PLAYER_MAX_THRUST) thrust=PLAYER_MAX_THRUST;
	else if(thrust<0) thrust=0;

	this->thrust=thrust;

	if(this->particle_effect && 0<this->thrust) {	// release thrust particles, 3 at a time
		double deg=this->direction*180/M_PI + 180;
		double spread;
		
//		std::cout << "direction: " << deg << "\n";

		for(int i=0;i<3;++i) {
			Gfx_Point pdelta;
			spread=(rand()%30)-15+deg;
			spread*=(M_PI/180);

			pdelta.x=sin(spread)*0.004 + this->delta.x;
			pdelta.y=-cos(spread)*0.004 + this->delta.y;

			this->part->add(this->location,pdelta,0);
		}
	}
}

void Player::addDirection(double delta) {
	this->direction+=delta;
	if(this->direction>M_PI*2) this->direction-=M_PI*2;
	else if(this->direction<0) this->direction+=M_PI*2;
}

void Player::setDirection(double direction) {
	this->direction=direction;
}

void Player::calcScale() {
	for(int i=0;i<PLAYER_NUM_VERTICES;++i) {
		this->p_orig[i].x=this->p_orig[i].x*this->scale;
		this->p_orig[i].y=this->p_orig[i].y*this->scale;
	}
}

void Player::calcProj() {
	for(int i=0;i<PLAYER_NUM_VERTICES;++i) {
		this->p_rot[i].x=this->p_orig[i].x * cos(this->direction) - this->p_orig[i].y * sin(this->direction);
		this->p_rot[i].y=(this->p_orig[i].y * cos(this->direction) + this->p_orig[i].x * sin(this->direction)) * g->aspect_ratio;
	}
}

void Player::update() {

	double a,d,xd,yd;
	xd=this->location.x - this->w->moon->location.x;	// TODO Also player doesn't need to know where moon is
	yd=this->location.y - this->w->moon->location.y;	// gravity and collision detection somewhere else please.

	a=atan2(yd, xd) - M_PI_2;
	d=sqrt(xd*xd+yd*yd);
	this->delta.x+=sin(a)* (w->moon->gravity*(1.2-d));
	this->delta.y-=cos(a)* (w->moon->gravity*(1.2-d));

	if(this->thrust>0) {
		this->delta.x+=sin(this->direction)*this->thrust;
		this->delta.y-=cos(this->direction)*this->thrust;
	}

	this->location.x+=this->delta.x;
	this->location.y+=this->delta.y;
	if(this->location.x < 0) { this->location.x += 1; this->delta.x*=w->border_speed_penalty; }
	if(this->location.y < 0) { this->location.y += 1; this->delta.y*=w->border_speed_penalty; }
	if(this->location.x > 1) { this->location.x -= 1; this->delta.x*=w->border_speed_penalty; }
	if(this->location.y > 1) { this->location.y -= 1; this->delta.y*=w->border_speed_penalty; }
	this->calcProj();
}

void Player::render() {
	SDL_Point points[PLAYER_NUM_VERTICES];

	if(this->particle_effect) this->part->render();

	SDL_SetRenderDrawBlendMode(g->renderer,SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(g->renderer,this->color.r,this->color.g,this->color.b,this->color.a);

	for(int i=0;i<PLAYER_NUM_VERTICES;++i) {	// Screen projection
		points[i].x=(int)((this->p_rot[i].x+this->location.x)*g->screen.w);
		points[i].y=(int)((this->p_rot[i].y+this->location.y)*g->screen.h);
	}

	SDL_RenderDrawLines(g->renderer,points,PLAYER_NUM_VERTICES);
}
