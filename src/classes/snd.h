#ifndef snd_h
#define snd_h

#include "../SDL_includet.h"

#define AUDIO_CHANNELS			16
#define AUDIO_RESERVED_CHS		8
#define AUDIO_SHIP_ENGINE_CH	0
#define NUM_SOUNDS				5

#define SOUND_ENGINE			0
#define SOUND_GAMEOVER			1
#define SOUND_BOUNCE			2
#define SOUND_START 			3
#define SOUND_EXPLOSION			4

class Snd {
public:
	bool sndena = false;
	Mix_Music* music;
	Mix_Chunk* mixchunk[NUM_SOUNDS] = {0};
	int sndchan[NUM_SOUNDS] = {AUDIO_SHIP_ENGINE_CH, -1, -1, -1, -1};

	Snd();
	~Snd();

	void Play(int soundnum);
	void Halt(int soundnum);
};

#endif
