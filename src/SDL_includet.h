#ifndef sdl_includet_h
#define sdl_includet_h

#if (_MSC_VER > 0) // tunnistaa ollaanko Visual Studiossa
#define _USE_MATH_DEFINES
#include <math.h>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>
//#include <SDL2_gfxPrimitives.h>
#include <cstdlib>
#else // linux
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
//#include <SDL2/SDL2_gfxPrimitives.h>
#endif
#endif

