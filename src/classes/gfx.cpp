#include <iostream>
#include "gfx.h"
#include "../korc_waves.h"


Gfx::Gfx(Uint32 flags) {

	std::cout << "Start GFX init...\n SDL2... " << std::flush;
	if(SDL_Init(flags)<0)
	{
		std::cout << "failed: " << SDL_GetError();
		std::cout << "\n";
		this->init_error += GFX_SDL_INIT_ERROR;
	}
	else
	{
// Init window
		// get screen size
		if(!SDL_GetDisplayBounds(0,&this->screen)) {
			this->screen_height=screen.h;
			this->screen_width=screen.w;
		}

		aspect_ratio = (double)screen_width / (double)screen_height;
		std::cout << "X:" << screen.w << " Y:" << screen.h << " AR:" << aspect_ratio << "\n";

		this->screen_center={ screen.w >> 1 , screen.h >> 1 };

		std::cout << "Ok!\n window... " << std::flush;
		this->window=SDL_CreateWindow("korc_waves", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, this->screen_width, this->screen_height, SDL_WINDOW_SHOWN|SDL_WINDOW_FULLSCREEN);
		if (this->window==NULL)
		{
			std::cout << "failed! SDL_Error: " << SDL_GetError();
			std::cout << "\n";
			this->init_error += GFX_SDL_WIN_ERROR;
		}
		else
		{
			std::cout << "Ok!\n renderer... " << std::flush;
			this->renderer=SDL_CreateRenderer(this->window, -1, SDL_RENDERER_ACCELERATED);
			if (this->renderer == NULL)
			{
				std::cout << "failed! SDL_Error: " << SDL_GetError();
				std::cout << std::endl;
				this->init_error+=GFX_SDL_RENDERER_ERROR;
			}
			else
			{
				SDL_SetRenderDrawColor(this->renderer, 0x00, 0x00, 0x00, SDL_ALPHA_OPAQUE);
				SDL_RenderClear(this->renderer);
				SDL_RenderPresent(this->renderer);
				std::cout << "Ok!\n";
			}
		}
	}
	std::cout << "Gfx Init " << (0==this->init_error?"":"in") << "complete.\n";
}

Gfx::~Gfx() {
 	std::cout << "Destroy Gfx...\n";

	if(0==(this->init_error&GFX_SDL_RENDERER_ERROR))
	{
		std::cout << " renderer destroyed.\n";
		SDL_DestroyRenderer(this->renderer);
	}

	if(0==(this->init_error&GFX_SDL_WIN_ERROR))
	{
		std::cout << " window destroyed.\n";
		SDL_DestroyWindow(this->window);
	}

	if(0==(this->init_error&GFX_SDL_INIT_ERROR))
	{
		std::cout << " SDL2 Quitted.\n";
		SDL_Quit();
	}
	std::cout << "Gfx destroyed!\n";
}

void Gfx::clear()
{
	SDL_SetRenderDrawBlendMode(this->renderer,SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(this->renderer,0x08,0x08,0x10,0xff);
	SDL_RenderFillRect(this->renderer,NULL);
}
