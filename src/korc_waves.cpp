/*
KORC_WAVES
by Juri Laihonen and Jan Hagqvist

Project started: 2017-01-20
at FGJ17 Kymenlaakso
*/

#include <iostream>
#include <sstream>

#include "SDL_includet.h"
#include "korc_waves.h"
#include "classes/gfx.h"
#include "classes/player.h"
#include "classes/snd.h"
#include "classes/moon.h"

int main(int argv,char *argc[]) {
	std::cout << "KORC_WAVES 2017-01-29\nBy Juri Laihonen and Jan Hagqvist\n";

	world w;
	Gfx *gfx=new Gfx(SDL_INIT_VIDEO);

	if(gfx->init_error) {
		std::cout << "gfx_init failed. (err:" << gfx->init_error << "). Cannot continue!\n";
		delete(gfx);
		exit(1);
	}

	Snd *snd=new Snd();
	Text *text = new Text(gfx);
	w.exit=false;
	w.gamestate=GAMESTATE_MENU;
	w.refresh_time=0;
	w.refresh_rate=20;
//	w.gravity=GRAVITY;
	w.ship1 = new Player(gfx, &w, snd, {0xff,0x40,0x40,0xff}, {0.20,0.5}, 0, {0, -0.01});
	w.ship2 = new Player(gfx, &w, snd, {0x60,0x60,0xff,0xff}, {0.80,0.5}, M_PI, {0, 0.01});
	w.moon = new Moon(gfx,&w);
	w.moon->gravity=MOON_GRAVITY;
	
	w.border_speed_penalty=OVER_BORDER_SPEED_PENALTY;
	w.bounce_factor=BOUNCE_FACTOR;

	w.moon->setLocation({0.5,0.5});
	
	w.ship1->restart();
	w.ship2->restart();
		
	while(!w.exit) {
        if(w.gamestate == GAMESTATE_MENU)
            mainmenu(&w, gfx, snd, text);
        if(w.gamestate == GAMESTATE_RUNNING)
            gameloop(&w, gfx, snd, text);
        if(w.gamestate == GAMESTATE_GAMEOVER)
            gameover(&w, gfx, snd, text);
	}

	delete(w.moon);
	delete(w.ship1);
	delete(w.ship2);
	delete(text);
	delete(snd);
	delete(gfx);

	return 0;
}

void gameloop(world *w, class Gfx *gfx, class Snd *snd, class Text *text)
{
	SDL_Event e;
	const Uint8 *keystate;
	std::stringstream str;

	if((abs(w->ship1->location.x - w->ship2->location.x) < 0.03) && (abs(w->ship1->location.y - w->ship2->location.y) < 0.03))
  {
		Gfx_Point tmploc;
		snd->Play(SOUND_BOUNCE);
		tmploc = w->ship1->delta;
		w->ship1->delta = w->ship2->delta;
		w->ship2->delta = tmploc;

		w->ship1->delta = { w->ship1->delta.x*w->bounce_factor, w->ship1->delta.y*w->bounce_factor};
		w->ship2->delta = { w->ship2->delta.x*w->bounce_factor, w->ship2->delta.y*w->bounce_factor};
	}

	if((abs(w->ship1->location.x - w->moon->location.x) < GFX_MOON_DIAMETER) && (abs(w->ship1->location.y - w->moon->location.y) < GFX_MOON_DIAMETER)) {
		w->winner=PLAYER_BLUE;
		w->gamestate = GAMESTATE_GAMEOVER;
		snd->Play(SOUND_GAMEOVER);
	}

	if((abs(w->ship2->location.x - w->moon->location.x) < GFX_MOON_DIAMETER) && (abs(w->ship2->location.y - w->moon->location.y) < GFX_MOON_DIAMETER)) {
		w->winner=PLAYER_RED;
		w->gamestate = GAMESTATE_GAMEOVER;
		snd->Play(SOUND_GAMEOVER);
	}

	w->ship1->update();
	w->ship2->update();
	w->moon->update();

	gfx->clear();

	w->moon->render();
	w->ship1->render();
	w->ship2->render();

	str.str("");
	str << "FUEL: ";
	str << (int)w->ship1->fuel;
	text->rendertext(str.str(), 0.02, 0.04, false);
	str.str("");
	str << "FUEL: ";
	str << (int)w->ship2->fuel;
	text->rendertext(str.str(), 0.82, 0.04, false);

    while(w->refresh_time>SDL_GetTicks()) { SDL_Delay(1); }
    w->refresh_time=SDL_GetTicks()+w->refresh_rate;
    SDL_RenderPresent(gfx->renderer);

    while(SDL_PollEvent(&e)) {

        switch(e.type) {

            case SDL_KEYDOWN:	
                switch(e.key.keysym.scancode) {

                    case SDL_SCANCODE_ESCAPE:
                        w->exit=1;
                        break;
                        
                    default:break;
                }
                break;

            case SDL_QUIT:
                w->exit=true;
                break;

            default:break;
        }
    }

// repeated key handler

    keystate=SDL_GetKeyboardState(NULL);

    if(keystate[SDL_SCANCODE_A]) { w->ship1->addDirection(-PLAYER_DIRECTION_DELTA); }
    if(keystate[SDL_SCANCODE_S]) { w->ship1->addDirection(PLAYER_DIRECTION_DELTA); }
    if(keystate[SDL_SCANCODE_LSHIFT]) {
		if(w->ship1->fuel>0)
		{
			w->ship1->setThrust(PLAYER_MAX_THRUST);
			w->ship1->fuel -= 0.2;
		}
    }	else {
        w->ship1->setThrust(0);
    }

    if(keystate[SDL_SCANCODE_K]) { w->ship2->addDirection(-PLAYER_DIRECTION_DELTA); }
    if(keystate[SDL_SCANCODE_L]) { w->ship2->addDirection(PLAYER_DIRECTION_DELTA); }
    if(keystate[SDL_SCANCODE_RSHIFT]) {
		if(w->ship2->fuel>0)
		{
			w->ship2->setThrust(PLAYER_MAX_THRUST);
			w->ship2->fuel -= 0.2;
		}
    }	else {
        w->ship2->setThrust(0);
    }
}

void mainmenu(world *w, class Gfx *gfx, class Snd *snd, class Text *text)
{
	SDL_Event e;
 	std::stringstream str;
	static double pos = 0;

	pos += 0.025;
	w->ship1->update();
	w->ship2->update();
	w->moon->update();

	gfx->clear();

	w->moon->render();
	w->ship1->render();
	w->ship2->render();
	
	str.str("");
	str << "KORC WAVES";
	text->rendertext(str.str(), -1, 0.15 + sin(pos)/32, true);
	str.str("");
	str << "Avoid the moon";
	text->rendertext(str.str(), -1, 0.35, false);
	str.str("");
	str << "RED controls A, S, LSHIFT";
	text->rendertext(str.str(), -1, 0.50, false);
	str.str("");
	str << "BLUE controls K, L, RSHIFT";
	text->rendertext(str.str(), -1, 0.57, false);
	str.str("");
	str << "Press RETURN to start";
	text->rendertext(str.str(), -1, 0.70, false);
	str.str("");
	str << "#fgj17 Kymenlaakso, #ggj17, Team JL, JH.";
	text->rendertext(str.str(), -1, 0.85, false);
	str.str("");
	str << "2017";
	text->rendertext(str.str(), -1, 0.92, false);

	while(w->refresh_time>SDL_GetTicks()) { SDL_Delay(1); }
	w->refresh_time=SDL_GetTicks()+w->refresh_rate;
	SDL_RenderPresent(gfx->renderer);
	
	while(SDL_PollEvent(&e)) {

		switch(e.type) {

			case SDL_KEYDOWN:	
				switch(e.key.keysym.scancode) {

					case SDL_SCANCODE_ESCAPE:
						w->exit=1;
						break;

					case SDL_SCANCODE_RETURN:
						w->ship1->restart();
						w->ship2->restart();
						w->gamestate = GAMESTATE_RUNNING;
						snd->Play(SOUND_START);
						break;
                        
					default:break;
				}
			break;

			case SDL_QUIT:
				w->exit=true;	
				break;

			default:break;
		}
	}
}

void gameover(world *w, class Gfx *gfx, class Snd *snd, class Text *text)
{
	SDL_Event e;
//    const Uint8 *keystate;
	std::stringstream str;
	static bool explosion = false;
	
	if(w->winner == PLAYER_BLUE)
	{
		w->ship2->update();
		if(w->ship1->location.y < 0.95)
		{
			w->ship1->addDirection(PLAYER_DIRECTION_DELTA * 4);
			w->ship1->location.y += (1 - w->ship1->location.y) / 20;
			w->ship1->calcProj();
		}
		else
		{
			if(!explosion)
			{
				explosion = true;
				snd->Play(SOUND_EXPLOSION);
			}
		}
	}
	if(w->winner == PLAYER_RED)
	{
		w->ship1->update();
		if(w->ship2->location.y < 0.95)
		{
			w->ship2->addDirection(PLAYER_DIRECTION_DELTA * 4);
			w->ship2->location.y += (1 - w->ship2->location.y) / 20;
			w->ship2->calcProj();
		}
		else
		{
			if(!explosion)
			{
				explosion = true;
				snd->Play(SOUND_EXPLOSION);
			}
		}
	}
	w->moon->update();

	gfx->clear();

	w->moon->render();
	w->ship1->render();
	w->ship2->render();
	
	str.str("");
	str << "GAME OVER";
	text->rendertext(str.str(), -1, 0.20, true);
	str.str("");
	if(w->winner == PLAYER_RED)
		str << "RED";
	else
		str << "BLUE";
	str << " was victorious!";
	text->rendertext(str.str(), -1, 0.40, false);
	str.str("");
	str << "Press RETURN for menu";
	text->rendertext(str.str(), -1, 0.60, false);

	while(w->refresh_time>SDL_GetTicks()) { SDL_Delay(1); }
	w->refresh_time=SDL_GetTicks()+w->refresh_rate;
	SDL_RenderPresent(gfx->renderer);

	while(SDL_PollEvent(&e)) {

		switch(e.type) {

			case SDL_KEYDOWN:	
				switch(e.key.keysym.scancode) {

					case SDL_SCANCODE_ESCAPE:
						w->exit=1;
						break;

					case SDL_SCANCODE_RETURN:
						explosion = false;
						w->gamestate = GAMESTATE_MENU;
						break;
                        
					default:break;
				}
				break;

			case SDL_QUIT:
				w->exit=true;	
				break;

			default:break;
		}
	}
}
