#ifndef gfx_h
#define gfx_h

typedef struct {
	double x;
	double y;
} Gfx_Point;

#include "../SDL_includet.h"
#include "../korc_waves.h"

#define GFX_SDL_INIT_ERROR 1
#define GFX_SDL_WIN_ERROR 2
#define GFX_SDL_RENDERER_ERROR 4

class Gfx {
public:
	SDL_Rect screen;
	Uint32 init_error;
	SDL_Renderer *renderer;
	SDL_Window *window;
	Uint16 screen_width;
	Uint16 screen_height;
	SDL_Point screen_center;
	double aspect_ratio;
	int framecnt;
  //  void* wo;
    Gfx(Uint32 flags);
	~Gfx();
	void clear();
	Uint32 init(Uint32 flags);	
};


#endif
