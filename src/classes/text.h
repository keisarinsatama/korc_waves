#ifndef text_h
#define text_h

#include <string>
#include "../SDL_includet.h"
#include "gfx.h"

class Text {
public:
	Gfx *g;
	int fontsize;
	TTF_Font *font;
	Text(Gfx *g);
	~Text();
	void rendertext(std::string txt, double x, double y, bool flash);
};

#endif
