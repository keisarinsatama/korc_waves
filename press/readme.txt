KORC WAVES

A two player space ship game where each player controls their own space ship.

OBJECTIVE: Avoid hitting the moon.

You can hit the other player to bounce them, but beware of losing control yourself.


- Uses SDL2 libraries (SDL2, SDL2_mixer, SDL2_ttf, SDL2_image)
- Sounds (CC0) made with http://sfbgames.com/chiptone/
- Moon image (CC0) from https://pixabay.com/en/jupiter-planet-galilean-monde-io-11615/
- Font Aileron (CC0) 
- Music (CC PD) http://freemusicarchive.org/music/Lee_Rosevere/Red_Danube/LeeRosevere_RedDanube


