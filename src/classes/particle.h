#ifndef particle_h
#define particle_h

#include "../SDL_includet.h"
#include "../korc_waves.h"
#include "gfx.h"
#define MAX_PARTICLE_COUNT 1000

struct particle {
	Gfx_Point location;
	Gfx_Point delta;
	unsigned int update_count;
	unsigned char is_visible;
	unsigned char type;
	float opacity;
};

class Particle {
	unsigned int index;
	Uint32 deploy_next;		// Seuraavan partikkelinlisäysmahdollisuuden aikaleima.
	class Gfx *g;
	struct world *w;
public:
	particle *part;	// Partikkelipuskuri

	unsigned int max_particle_count;
	unsigned int init_error;			// Jos puskurin varaus epäonnistuu, tähän tulee 1.
	unsigned int fade_speed;		// Kuinka nopeasti partikkeli häivytetään... jotenki. mystisesti...
	float fade_delta;
	unsigned char fill_mode;	// 0 = vanhimman ylikirjoitus, 1 = ei lisätä jos puskuri täynnä loppuun asti näytttämättömiä partikkeleita.
	Uint32 deploy_rate;
	
	Particle(Gfx *g,struct world *w);
	Particle(Gfx *g,struct world *w,unsigned int max_particle_count);
	~Particle();
	void init();
	void clearAll();
	void setFadeSpeed(int fspeed);
	int add(Gfx_Point origin,Gfx_Point delta,unsigned int type);	// Lisää uuden partikkelin puskuriin.
	void update();
	void render();
};
#endif
