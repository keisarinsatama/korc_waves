#ifndef moon_h
#define moon_h

#define GFX_MOON_DIAMETER 0.05
#define MOON_GRAVITY 0.0003

#include "../korc_waves.h"
#include "gfx.h"

class Moon {
	world *w;
	Gfx *g;

	int framecnt;
	SDL_Texture *texture;
	SDL_Surface *surface;

public:
	Gfx_Point location;
	double gravity;

Moon(Gfx *g,world *w);
~Moon();

void setLocation(const Gfx_Point location);
Gfx_Point getLocation();
void update();
void render();

};

#endif
