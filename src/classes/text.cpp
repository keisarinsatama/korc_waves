#include <iostream>
#include <string>
#include "text.h"

// TODO This is really something we must do more efficiently.

Text::Text(Gfx *g)
{
	this->g = g;
	fontsize = g->screen_height * 0.05;
	TTF_Init();
	std::cout << "TTF init... ";
	font=TTF_OpenFont("assets/Aileron-SemiBold.ttf", fontsize);
	
	std::cout << "OK\n";
}

Text::~Text()
{
	if(this->font)
		TTF_CloseFont(this->font);

	TTF_Quit();
}

void Text::rendertext(std::string txt, double x, double y, bool flash)
{
	static int col = 120, xc = 1;
	SDL_Color fg = {(Uint8)col, (Uint8)col, (Uint8)col, 255};
	SDL_Surface *surf;
	SDL_Texture *tex;
	SDL_Rect destrect;

	if((col < 120) || (col > 136))
		xc=-xc;
	if(!flash)
		fg = {192, 192, 192, 255};
	surf = TTF_RenderText_Solid(this->font, txt.c_str(), fg);
	if(x != -1)
		destrect.x = x * g->screen_width;
	else
		destrect.x = (g->screen_width - surf->w) / 2;
	if(y != -1)
		destrect.y = y * g->screen_height;
	else
		destrect.y = (g->screen_height - surf->h) / 2;
	destrect.w = surf->w;
	destrect.h = surf->h;
	tex = SDL_CreateTextureFromSurface(g->renderer, surf);
	SDL_RenderCopy(g->renderer, tex, NULL, &destrect);
	SDL_DestroyTexture(tex);
	SDL_FreeSurface(surf);
	col += xc;
}
