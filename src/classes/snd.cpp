#include <iostream>
#include "snd.h"

// inits sounds
Snd::Snd()
{
	int init = Mix_Init(MIX_INIT_OGG);
    
    std::cout << "Snd init... ";

	if ((init&MIX_INIT_OGG) != MIX_INIT_OGG)
	{
		std::cout << "Sound init failed: " << Mix_GetError();
		sndena = false;
	}
	else
	{
		if ((init = Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096)) == 0)
		{
			Mix_AllocateChannels(AUDIO_CHANNELS); // Amount of mixed sound channels
			Mix_ReserveChannels(AUDIO_RESERVED_CHS); // How many channels to reserve for 'play on first free channel' (channel -1)
			mixchunk[SOUND_BOUNCE] = Mix_LoadWAV("assets/bounce.wav");
			mixchunk[SOUND_START] = Mix_LoadWAV("assets/start.wav");
			mixchunk[SOUND_GAMEOVER] = Mix_LoadWAV("assets/gameover.wav");
			mixchunk[SOUND_EXPLOSION] = Mix_LoadWAV("assets/explosion.wav");
			
			music = Mix_LoadMUS("assets/Lee_Rosevere_-_01_-_Red_Danube.ogg");
			std::cout << "MUS" << music << Mix_GetError() << std::flush;
			Mix_VolumeMusic(MIX_MAX_VOLUME * 0.5);
			Mix_PlayMusic(music, -1);
			sndena = true;
		}
		else
		{
			std::cout << "Unable to init sound source: " << Mix_GetError();
			sndena = false;
		}
	}
	
	std::cout << " OK\n";
}

Snd::~Snd()
{
	if(music)
	{
		Mix_HaltMusic();
		Mix_FreeMusic(music);
	}
	
	for (int i = 0; i < NUM_SOUNDS; i++)
		Mix_FreeChunk(mixchunk[i]);

	Mix_CloseAudio();
}

// plays a sound, soudnum is the number of the sound
void Snd::Play(int soundnum)
{
	if(soundnum>=0 && soundnum<=NUM_SOUNDS)
	{
		if(sndchan[soundnum]==-1) // sound can be on any channel
			Mix_PlayChannel(sndchan[soundnum], mixchunk[soundnum], 0);
		else // sound has to be on a specific channel
		{
			if(Mix_Playing(sndchan[soundnum]) == 0)
			{
				Mix_Volume(sndchan[soundnum], 64);
				Mix_PlayChannel(sndchan[soundnum], mixchunk[soundnum], -1);
			}
		}
	}
}

// stops a sound, used on looping sounds like engine sound
void Snd::Halt(int soundnum)
{
	if(soundnum>=0 && soundnum<=NUM_SOUNDS)
		Mix_HaltChannel(sndchan[soundnum]);
}

