#include <iostream>
#include <math.h>
#include <memory.h>
#include <stdlib.h>

#include "particle.h"

Particle::Particle(Gfx *g,world *w) {
	this->g=g;
	this->w=w;
	this->init();
	this->part=(struct particle *)malloc(sizeof(struct particle)*this->max_particle_count);
	if(!this->part) this->init_error=1;
	else this->clearAll();
}

Particle::Particle(Gfx *g,world *w,unsigned int max_particle_count) {
	this->g=g;
	this->w=w;
	this->init();
	this->max_particle_count=max_particle_count;
	this->part=(struct particle *)malloc(sizeof(struct particle)*this->max_particle_count);
	if(!this->part) this->init_error=1;
	else this->clearAll();
}

Particle::~Particle() {
	free(this->part);
}

void Particle::clearAll() {
	for(int i=0;i<this->max_particle_count;++i) {
		this->part[i].delta.x=0.0;
		this->part[i].delta.y=0.0;
		this->part[i].is_visible=0;
		this->part[i].type=0;
		this->part[i].update_count=0;
		this->part[i].location.x=0.0;
		this->part[i].location.y=0.0;
		this->part[i].opacity=255;
	}
}
void Particle::init() {
	this->init_error=0;
	this->max_particle_count=MAX_PARTICLE_COUNT;
	this->index=0;
	this->deploy_next=0;
	this->deploy_rate=0;
	this->setFadeSpeed(100);
	this->part=NULL;
	this->fill_mode=0;
}

void Particle::setFadeSpeed(int fspeed) {
	this->fade_speed=fspeed;
	this->fade_delta=255.0/(float)fspeed;
}

int Particle::add(const Gfx_Point origin,const Gfx_Point delta,const unsigned int type) {
	if(1==fill_mode && part[this->index].is_visible) return 0;
	if(this->deploy_rate) {
		if(this->deploy_next<=SDL_GetTicks()) {
			this->deploy_next=SDL_GetTicks()+this->deploy_rate;
		}
		else return 0;
	}
	this->part[this->index].location=origin;
	this->part[this->index].delta=delta;
	this->part[this->index].update_count=0;
	this->part[this->index].is_visible=1;
	this->part[this->index].type=type;
	this->part[this->index].opacity=255;

	if(++this->index>=this->max_particle_count) this->index=0;

	return 0;
}

// Lasketaan partikkelin uudet koordinaatit ja muut
//
void Particle::update() {
	double a,d; // angle, distance
	double xd,yd;
	for(int i=0;i<this->max_particle_count;++i) {
		if(!part[i].is_visible) continue;	// partikkeli "käytetty loppuun", ei päivitetä.

		if(++part[i].update_count>fade_speed) {	// partikkeli käytettiin loppuun, merkataan poistetuksi, ei päivitetä.
			part[i].is_visible=0;
			continue;
		}

		// 0 = g-force vaikuttaa.
		// 1 = g-force vaikutus voidaan ohittaa. (valoammukset ym)
		switch(part[i].type) {
			case 0:
				part[i].location.x+=part[i].delta.x;
				part[i].location.y+=part[i].delta.y;

				xd=part[i].location.x - this->w->moon->location.x;	// TODO moon gravity and collision claculation somewhere else please.
				yd=(part[i].location.y - this->w->moon->location.y) / this->g->aspect_ratio;	// it is not particles job to know where the moon is.

				d=sqrt(yd*yd+xd*xd);
				if(d<GFX_MOON_DIAMETER/2) {	// Hit moon, disable particle.
					part[i].delta.x*=0.2;
					part[i].delta.y*=0.2;
					part[i].opacity*=0.85;
					if(part[i].opacity<10)
						part[i].is_visible=0;
					break;
				}

				a=atan2(yd, xd) - M_PI_2;
				part[i].delta.x+=sin(a)* (this->w->moon->gravity * (1.2-d));
				part[i].delta.y-=cos(a)* (this->w->moon->gravity * (1.2-d)) * this->g->aspect_ratio;
//				part[i].opacity-=this->fade_delta;
				break;

			case 1:
				part[i].location.x+=part[i].delta.x;
				part[i].location.y+=part[i].delta.y * this->g->aspect_ratio;
				break;
			default:break;
		}

		if(part[i].location.x>1) part[i].location.x-=1;
		if(part[i].location.x<0) part[i].location.x+=1;
		if(part[i].location.y>1) part[i].location.y-=1;
		if(part[i].location.y<0) part[i].location.y+=1;
	}
}

void Particle::render() {
	this->update();
	for(int i=0;i<this->max_particle_count;++i)
	{
		if(!this->part[i].is_visible) continue;

		switch(this->part[i].type)
		{
			case 0:
				SDL_SetRenderDrawBlendMode(g->renderer, SDL_BLENDMODE_BLEND);
				SDL_SetRenderDrawColor(g->renderer, 0xff, 0xff, 0x00, (int)this->part[i].opacity);
				SDL_RenderDrawPoint(g->renderer,(int)(this->part[i].location.x*g->screen_width),(int)(this->part[i].location.y*g->screen_height));
				break;
			case 1:
				SDL_SetRenderDrawBlendMode(g->renderer, SDL_BLENDMODE_NONE);
				SDL_SetRenderDrawColor(g->renderer, 0xff, 0xff, 0x00, SDL_ALPHA_OPAQUE);
				SDL_RenderDrawPoint(g->renderer,(int)(this->part[i].location.x*g->screen_width),(int)(this->part[i].location.y*g->screen_height));
				break;
			default:break;
		}
	}
}
