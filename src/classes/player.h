#ifndef player_h
#define player_h

#include "../SDL_includet.h"
#include "../korc_waves.h"
#include "gfx.h"
#include "snd.h"
#include "particle.h"

#define PLAYER_NUM_VERTICES 5
#define PLAYER_MAX_THRUST 0.0005
#define PLAYER_DIRECTION_DELTA M_PI/36

class Player {
	Gfx_Point p_orig[PLAYER_NUM_VERTICES];
	Gfx_Point p_rot[PLAYER_NUM_VERTICES];
	SDL_Color color;
	class Gfx *g;
	Snd *snd;
	struct world *w;
	class Particle *part;
	bool particle_effect;
	
	double scale;
	double direction;

public:
	Gfx_Point start_location;
	Gfx_Point start_delta;
	Gfx_Point location;
	Gfx_Point delta;
	double orientation; // radians
	double thrust;
	double fuel;
	double start_dir;
	Player(Gfx *g,struct world *w,Snd *snd,SDL_Color color,Gfx_Point start_location, double start_dir, Gfx_Point start_delta);
	~Player();

	void restart();
	void update();
	void setScale(double scale);
	void calcScale();
	void setThrust(double);
	void addDirection(double);
	void setDirection(double);
	void calcProj();
	void render();
};

#endif
