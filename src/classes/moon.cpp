#include "moon.h"

Moon::Moon(Gfx *g,world *w) {
		this->w=w;
		this->g=g;
		framecnt=0;
		this->location.x=(double)0.5;
		this->location.y=(double)0.5;
		this->surface = IMG_Load ("assets/moon.png");
		this->texture = SDL_CreateTextureFromSurface (g->renderer, this->surface);
		SDL_SetTextureBlendMode (this->texture, SDL_BLENDMODE_BLEND);
		if (surface != NULL)
			SDL_FreeSurface (surface);
}

Moon::~Moon() {
	if(this->texture) {
		SDL_DestroyTexture(this->texture);
	}
}

void Moon::setLocation(const Gfx_Point location) {
	this->location=location;
}

Gfx_Point Moon::getLocation() { return this->location; }

void Moon::update() {
	++this->framecnt;
	this->location.y = 0.5 + sin((double)framecnt / 20) / 16;
	this->location.x = 0.5 + cos((double)framecnt / 20) / 16;
}

void Moon::render() {
	SDL_Rect destrect;
	destrect = {(int)((this->location.x - GFX_MOON_DIAMETER / 2) * g->screen_width), (int)((this->location.y - GFX_MOON_DIAMETER * g->aspect_ratio / 2) * g->screen_height), (int)(GFX_MOON_DIAMETER * g->screen_width), (int)(GFX_MOON_DIAMETER * g->aspect_ratio * g->screen_height)};
	SDL_RenderCopyEx(g->renderer, this->texture, NULL, &destrect, sin((double)framecnt/200)*360, NULL, SDL_FLIP_NONE);
}
